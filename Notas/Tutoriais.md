# Phaser

# Tutoriais
- [Game Dev Academy](https://gamedevacademy.org/create-a-basic-multiplayer-game-in-phaser-3-with-socket-io-part-1/);
- [Top Down Shooter](https://phaser.io/tutorials/coding-tips-007)
- [Tutorial 1 - eEmanuele Feronato](https://www.emanueleferonato.com/);
- [Dont Touch The Spike](https://phaser.io/news/2019/02/dont-touch-the-spikes-tutorial);
- [Ourcade](https://blog.ourcade.co/);
- [Ourcade (PDF)](http://assets.ourcade.co/books/Infinite_Jumper_Phaser3_Modern_JavaScript.pdf);
- [Ourcade (YT)](https://www.youtube.com/c/Ourcadehq/playlists);
- [Wild! (YT)](https://www.youtube.com/channel/UCv0j-6tXIlnxmOu9FA3qFtw);
- [Zenva (YT)](https://www.youtube.com/playlist?list=PLnEt5PBXuAmtoTvwnF6Ksj7qy1JZHn5Nn)
- [Rode Antunes (YT)](https://www.youtube.com/c/RodrigoAntunesroddyka/videos);

## Exemplos
- [Space Invaders](https://phaser.io/examples/v2/games/invaders);
- [Pinball](https://phaser.io/examples/v2/box2d/pinball);
- [Pimbal](https://testdrive-archive.azurewebsites.net/Graphics/CanvasPinball/default.html);

## UI
- [ProgressBar](https://gamedevacademy.org/creating-a-preloading-screen-in-phaser-3/);
