# Tutorial

Importar
> 	<script src="../../../Scripts/Engines/Phaser.js"></script>

[Phaser](https://phaser.io/)

## Doc:

- https://developer.mozilla.org/en-US/docs/Games/Tutorials

## Videos:

- [Canal Wild](https://www.youtube.com/channel/UCv0j-6tXIlnxmOu9FA3qFtw/playlists)

- [ ] [TT1](https://www.youtube.com/watch?v=frRWKxB9Hm0&list=PLCw7wwjGEim4xTHUNLqp2JDgqio4Rh78l)
- [ ] [TT2](https://www.youtube.com/watch?v=IQs_pze2SsA&list=PL9iYZZWgVwsfd3z_wowmPYNOLOkqTvTRd)
- [ ] [TT3](https://www.youtube.com/watch?v=fdXcD9X4NrQ)
- [ ] [TT4](https://www.youtube.com/watch?v=EsZGoHtt1jo)

---

# Download

- Template
  - Template GitHub
    > git clone https://github.com/photonstorm/phaser3-project-template.git nome
- Download ficheiro
  - [Phaser.js](http://phaser.io/download/stable)
- NPM:
  - > criar antes um ficheiro npm init -y
  - > npm install --save phaser
  - No HTML:
    ```
    <body>
        <div id="root"></div>
        <script src="./node_modules/phaser/dist/phaser.js"></script>
        <script src="./src/animation.js"></script>
    </body>
    ```
