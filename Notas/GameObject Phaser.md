Game Object
===========

-	[Image](https://phaser.io/examples/v3/view/plugins/custom-game-object);
-	[Sprite](https://phaser.io/examples/v3/view/game-objects/images/custom-game-object);
-	[Container](https://phaser.io/examples/v3/view/game-objects/container/draggable-container);
-	[Constructor](https://phaser.io/docs/2.4.4/Phaser.GameObjectCreator.html);
