export const links = [
  {
    "name": "Box Physics Arcade",
    "web": "BoxPhysicsArcade",
  },
  {
    "name": "Car",
    "web": "Car",
  },
  {
    "name": "Colisao Bolas *",
    "web": "ColisaoBolas",
  },
  {
    "name": "Dino Star",
    "web": "DinoStar",
  },
  {
    "name": "Disparo",
    "web": "Disparo/Cima",
  },
  {
    "name": "Disparo 360",
    "web": "Disparo/Disparo360",
  },
  {
    "name": "Disparo Direction",
    "web": "Disparo/DisparoDirection",
  },
  {
    "name": "First Game",
    "web": "FirstGame",
  },
  {
    "name": "Grupo",
    "web": "Group",
  },
  {
    "name": "Luz *",
    "web": "Luz",
  },
  {
    "name": "Particle System *",
    "web": "ParticleSystem",
  },
  {
    "name": "Perlin Noise",
    "web": "PerlinNoise",
  },
  {
    "name": "Progress Bar",
    "web": "ProgressBar",
  },
  {
    "name": "Render Texture",
    "web": "RenderTexture",
  },
  {
    "name": "Scenas",
    "web": "Scenas",
  },
  {
    "name": "Spine",
    "web": "Spine",
  },
  {
    "name": "Sprite *",
    "web": "Sprite",
  },
  {
    "name": "Sprite Sheet",
    "web": "SpriteSheet",
  },
  {
    "name": "Tween *",
    "web": "Tween",
  },
];
